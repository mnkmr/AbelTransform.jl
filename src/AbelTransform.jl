module AbelTransform

export basex

using PyCall

const abel = PyNULL()

function __init__()
    copy!(abel, pyimport("abel"))
end


function basex(img::Array{<:Real,2}, centerpos="com")
    abel[:Transform](img, direction="inverse", method="basex", center=centerpos)[:transform]
end


end # module
